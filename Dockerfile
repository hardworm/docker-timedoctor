FROM ubuntu:16.04

ENV TIMEDOCTOR_USER=timedoctor

RUN apt-get update && apt-get install -y locales && rm -rf /var/lib/apt/lists/* \
    && localedef -i en_US -c -f UTF-8 -A /usr/share/locale/locale.alias en_US.UTF-8
ENV LANG en_US.utf8

RUN apt-get update && apt-get install -y -qq libx11-xcb1 libxcb1 libssl1.0.0 libqtcore4 libqtgui4 libqt4-dbus libqt4-network libqt4-sql libqt4-sql-sqlite libqt4-svg libqt4-xml libqt4-xmlpatterns gksu gdebi libjpeg8 libpng12-0 libcv2.4 libxcb-record0

COPY timedoctorpro_1.4.75-14ubuntu16.04_amd64.deb /tmp/
RUN dpkg -i /tmp/timedoctorpro_1.4.75-14ubuntu16.04_amd64.deb
RUN rm -rf /tmp/timedoctorpro_1.4.75-14ubuntu16.04_amd64.deb

COPY scripts/ /var/cache/timedoctor/
COPY entrypoint.sh /sbin/entrypoint.sh
RUN chmod 755 /sbin/entrypoint.sh

ENTRYPOINT ["/sbin/entrypoint.sh"]
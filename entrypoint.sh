#!/bin/bash
set -e

USER_UID=${USER_UID:-1000}
USER_GID=${USER_GID:-1000}

install_timedocotor() {
  echo "Installing timedoctor-wrapper..."
  install -m 0755 /var/cache/timedoctor/timedoctor-wrapper /target/
  echo "Installing timedoctor..."
  ln -sf timedoctor-wrapper /target/timedoctorpro
}

uninstall_timedoctor() {
  echo "Uninstalling timedoctor-wrapper..."
  rm -rf /target/timedoctor-wrapper
  echo "Uninstalling timedoctor..."
  rm -rf /target/timedoctorpro
}

create_user() {
  # create group with USER_GID
  if ! getent group ${TIMEDOCTOR_USER} >/dev/null; then
    groupadd -f -g ${USER_GID} ${TIMEDOCTOR_USER} >/dev/null 2>&1
  fi

  # create user with USER_UID
  if ! getent passwd ${TIMEDOCTOR_USER} >/dev/null; then
    adduser --disabled-login --uid ${USER_UID} --gid ${USER_GID} \
      --gecos 'Timedoctor' ${TIMEDOCTOR_USER} >/dev/null 2>&1
  fi
  chown ${TIMEDOCTOR_USER}:${TIMEDOCTOR_USER} -R /home/${TIMEDOCTOR_USER}
}

grant_access_to_video_devices() {
  for device in /dev/video*
  do
    if [[ -c $device ]]; then
      VIDEO_GID=$(stat -c %g $device)
      VIDEO_GROUP=$(stat -c %G $device)
      if [[ ${VIDEO_GROUP} == "UNKNOWN" ]]; then
        VIDEO_GROUP=skypevideo
        groupadd -g ${VIDEO_GID} ${VIDEO_GROUP}
      fi
      usermod -a -G ${VIDEO_GROUP} ${TIMEDOCTOR_USER}
      break
    fi
  done
}

launch_timedoctor() {
  cd /home/${TIMEDOCTOR_USER}
  exec sudo -HEu ${TIMEDOCTOR_USER} $@
}

case "$1" in
  install)
    install_timedocotor
    ;;
  uninstall)
    uninstall_timedoctor
    ;;
  timedoctorpro)
    create_user
#    grant_access_to_video_devices
    launch_timedoctor $@
    ;;
  *)
    exec $@
    ;;
esac
